﻿using Abp.Authorization;
using PKBOnline.Authorization.Roles;
using PKBOnline.Authorization.Users;

namespace PKBOnline.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
