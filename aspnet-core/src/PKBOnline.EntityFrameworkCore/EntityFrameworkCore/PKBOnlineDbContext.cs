﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using PKBOnline.Authorization.Roles;
using PKBOnline.Authorization.Users;
using PKBOnline.MultiTenancy;

namespace PKBOnline.EntityFrameworkCore
{
    public class PKBOnlineDbContext : AbpZeroDbContext<Tenant, Role, User, PKBOnlineDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public PKBOnlineDbContext(DbContextOptions<PKBOnlineDbContext> options)
            : base(options)
        {
        }
    }
}
