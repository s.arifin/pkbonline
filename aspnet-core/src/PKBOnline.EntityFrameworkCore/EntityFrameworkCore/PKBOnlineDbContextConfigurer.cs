using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace PKBOnline.EntityFrameworkCore
{
    public static class PKBOnlineDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<PKBOnlineDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<PKBOnlineDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
