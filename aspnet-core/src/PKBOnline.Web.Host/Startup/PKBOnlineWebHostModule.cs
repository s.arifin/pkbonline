﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using PKBOnline.Configuration;

namespace PKBOnline.Web.Host.Startup
{
    [DependsOn(
       typeof(PKBOnlineWebCoreModule))]
    public class PKBOnlineWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public PKBOnlineWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PKBOnlineWebHostModule).GetAssembly());
        }
    }
}
