﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PKBOnline.Authorization.Accounts.Dto;

namespace PKBOnline.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
