﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using PKBOnline.Configuration.Dto;

namespace PKBOnline.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : PKBOnlineAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
