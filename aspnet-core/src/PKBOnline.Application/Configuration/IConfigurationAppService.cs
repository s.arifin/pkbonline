﻿using System.Threading.Tasks;
using PKBOnline.Configuration.Dto;

namespace PKBOnline.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
